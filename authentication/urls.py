from django.urls import path
from .views import *

urlpatterns = [
    path('login_user', login_user),
    path('create_user_account', create_user_account),
    path('validate_otp', validate_otp),
    path('forget_password', forget_password),
    path('reset_password', reset_password),
    path('logout', logout_view)

]
