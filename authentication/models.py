from django.db import models
from clickup.models import BaseModel
from django.contrib.auth.models import AbstractUser


# Create your models here.


class Signup(BaseModel):
    PURPOSE_MENU = (
        ("SIGN_UP", "Account Sign Up"),
        ("FORGET_PASSWORD", "Forget Account Password")
    )

    email = models.EmailField()
    purpose = models.CharField(max_length=50, choices=PURPOSE_MENU)
    otp = models.CharField(max_length=4)
    is_utilized = models.BooleanField(default=False)

    def __str__(self):
        return self.email


class User(AbstractUser):
    task_capacity = models.PositiveIntegerField(null=True, blank=True)


class ValidateToken(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.TextField(editable=False)
    exp_time = models.DateTimeField()

    def __str__(self):
        return self.token
