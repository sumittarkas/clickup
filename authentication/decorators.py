import json
from django.http import JsonResponse

from clickup.datetime import get_now, timedelta
from django.conf import settings
from authentication.views import *

from functools import wraps
from wsgiref import headers
import jwt
from authentication.models import ValidateToken


def valid_token_required(function):
    def wrap(request, *args, **kwargs):
        try:
            headers = request.headers
            bearer_token = headers.get('Authorization')

            if not 'Authorization' in headers.keys():
                return JsonResponse({
                    "validation": "Auth Token missing",
                    "status": False, "redirect": "/"
                })

            if not bearer_token.split(' ')[0] == "Bearer":
                return JsonResponse({
                    "validation": "Invalid auth headers",
                    "status": False, "redirect": "/"})

            bearer_token = headers.get('Authorization')[7:]
            decoded_token = jwt.decode(bearer_token, settings.SECRET_KEY, algorithms="HS256")
            #user_id = User.objects.get(id=decoded_token.get('user_id'))
            user_id = decoded_token.get('user_id')
            try:

                User.objects.get(id=user_id)
            except User.DoesNotExist:
                return JsonResponse({
                    "validation": "Invalid user",
                    "status": False,
                    "status_code": 401,
                    "redirect": "/",
                })

            try:
                ValidateToken.objects.get(user__id=user_id, token=bearer_token)
            except ValidateToken.DoesNotExist:
                return JsonResponse({
                    "validation": "Forcefully logout",
                    "status": False,
                    "status_code": 401,
                    "redirect": "/",
                })

            return function(request, *args, **kwargs)
        except jwt.InvalidIssuerError:
            print("InvalidIssuerError")
            return JsonResponse({
                "validation": "Invalid issuer",
                "status": False,
                "status_code": 401,
                "redirect": "/",
            })
        except jwt.ExpiredSignatureError:
            print("ExpiredSignatureError")
            return JsonResponse({
                "validation": "Expired signature",
                "status": False,
                "status_code": 401,
                "redirect": "/",
            })
        except jwt.InvalidTokenError:
            print("InvalidTokenError")
            return JsonResponse({
                "validation": "Invalid token",
                "status": False,
                "status_code": 401,
                "redirect": "/",
            })

        except User.DoesNotExist:
            print("User.DoesNotExist")
            return JsonResponse({
                "validation": "Invalid user",
                "status": False,
                "status_code": 401,
                "redirect": "/",
            })

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap
