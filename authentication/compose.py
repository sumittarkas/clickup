import random
from clickup.settings import EMAIL_HOST_USER
from django.core.mail import send_mail
from django.core.paginator import Paginator


def generate_random_number(n):
    return random.randint(1111, 9999)


def trigger_email(subject, body, recipients):
    send_mail(subject, body, EMAIL_HOST_USER, recipients,
              fail_silently=False)


def pagination(objects, page_per_entries, page_number):
    pagination_dict = {}

    if not page_number:
        page_number = 1

    pagination_instance = Paginator(objects, page_per_entries)
    data_instance = pagination_instance.page(page_number)

    pagination_dict['total_entries'] = pagination_instance.count
    pagination_dict['total_pages'] = pagination_instance.num_pages if pagination_instance.count else 0
    pagination_dict['current_page'] = data_instance.number if pagination_instance.count else None
    pagination_dict['previous_page_number'] = data_instance.previous_page_number() if data_instance.has_previous() else None
    pagination_dict['next_page_number'] = data_instance.next_page_number() if data_instance.has_next() else None
    pagination_dict['start_page_index'] = data_instance.start_index()
    pagination_dict['end_page_index'] = data_instance.end_index()

    return data_instance.object_list, pagination_dict




