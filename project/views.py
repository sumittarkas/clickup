import json
from django.http import JsonResponse

from project.models import Company, Workspace, Project, List, TaskPriority, Task, SubTask, Setting
from clickup.datetime import get_now
from datetime import datetime, timedelta
import datetime
from base.compose import missing_fields_validation
from base.schemaValidator import validate_json
from project.schema import *

from django.contrib.auth.models import Permission, User
from authentication.compose import pagination
from django.contrib.contenttypes.models import ContentType

import xlrd


# Create your views here.
def company(request, pk=None):
    """
    Usage: create company.
    :params:
    title str

    :return:
    message: str
    status : True / False
    """
    if request.method == "POST":
        params = json.loads(request.body)
        schema = company_schema()
        json_error = validate_json(params, schema)
        if json_error: return JsonResponse(missing_fields_validation(json_error))

        queryset = Company.objects.filter(**params)

        if queryset.exists():
            return JsonResponse({
                "message": "Already available.",
                "status": False
            })

        instance = Company.objects.create(**params)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "GET":
        params = request.GET

        title = params.get("title")
        pk = params.get("id")
        page_number = params.get("page_number", 1)
        page_per_entries = params.get("page_per_entries", 5)
        kwargs = {}
        response = []

        if pk: kwargs['id'] = pk
        if title: kwargs["title"] = title

        queryset = Company.objects.filter(**kwargs)
        queryset, pagination_dict = pagination(queryset, page_per_entries, page_number)

        for instance in queryset:
            response.append(instance.get_json())

        return JsonResponse({
            "message": "Success",
            "status": True,
            "pagination": pagination_dict,
            "data": response,
        })

    if request.method == 'PATCH':
        params = json.loads(request.body)
        schema = company_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        title = params.get('title')
        kwargs = {}

        if title: kwargs['title'] = title

        Company.objects.filter(id=pk).update(**kwargs)
        instance = Company.objects.get(id=pk)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "DELETE":
        Company.objects.filter(id=pk).delete()

        return JsonResponse({
            "message": "Success",
            "status": True
        })


def workspace(request, pk=None):
    """
    Usage: create workspace title.
    :params:
    title str
    company_title str

    :return:
    message: str
    status : True / False

    """
    if request.method == "POST":
        params = json.loads(request.body)
        schema = workspace_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        workspace_title = params.get('title')
        company_title = params.get('company_title')
        company_object = Company.objects.get(title=company_title)

        queryset = Workspace.objects.filter(title=workspace_title)

        if queryset.exists():
            return JsonResponse({
                "name": "Already Available.",
                "status": False
            })

        instance = Workspace.objects.create(title=workspace_title, company=company_object)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "GET":
        params = request.GET

        title = params.get("title")
        pk = params.get("id")
        page_number = params.get("page_number", 1)
        page_per_entries = params.get("page_per_entries", 2)
        kwargs = {}
        response = []

        if pk: kwargs['id'] = pk
        if title: kwargs['title'] = title

        queryset = Workspace.objects.filter(**kwargs)
        queryset, pagination_dict = pagination(queryset, page_per_entries, page_number)

        for instance in queryset:
            response.append(instance.get_json())

        return JsonResponse({
            "message": "Success",
            "status": True,
            "pagination": pagination_dict,
            "data": response,
        })

    if request.method == 'PATCH':
        params = json.loads(request.body)
        schema = workspace_update_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        title = params.get('title')
        kwargs = {}

        if title: kwargs['title'] = title

        Workspace.objects.filter(id=pk).update(**kwargs)
        instance = Workspace.objects.get(id=pk)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "DELETE":
        Workspace.objects.filter(id=pk).delete()

        return JsonResponse({
            "message": "Success",
            "status": True
        })


def project(request, pk=None):
    """
    Usage: create project title.
    :params:
    title str
    workspace_title str

    :return:
    message: str
    status : True / False

    """

    if request.method == "POST":
        params = json.loads(request.body)
        schema = project_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        project_title = params.get('title')
        workspace_title = params.get('workspace_title')
        workspace_object = Workspace.objects.get(title=workspace_title)

        queryset = Project.objects.filter(title=project_title)

        if queryset.exists():
            return JsonResponse({
                "name": "Already Available.",
                "status": False
            })

        instance = Project.objects.create(title=project_title, workspace=workspace_object)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "GET":
        params = request.GET

        title = params.get("title")
        pk = params.get("id")
        page_number = params.get("page_number", 1)
        page_per_entries = params.get("page_per_entries", 2)
        kwargs = {}
        response = []

        if pk: kwargs['id'] = pk
        if title: kwargs["title"] = title

        queryset = Project.objects.filter(**kwargs)
        queryset, pagination_dict = pagination(queryset, page_per_entries, page_number)

        for instance in queryset:
            response.append(instance.get_json())

        return JsonResponse({
            "message": "Success",
            "status": True,
            "pagination": pagination_dict,
            "data": response,
        })

    if request.method == 'PATCH':
        params = json.loads(request.body)
        schema = project_update_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        title = params.get('title')
        kwargs = {}

        if title: kwargs['title'] = title

        Project.objects.filter(id=pk).update(**kwargs)
        instance = Project.objects.get(id=pk)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "DELETE":
        Project.objects.filter(id=pk).delete()

        return JsonResponse({
            "message": "Success",
            "status": True
        })


def list_element(request, pk=None):
    """
    Usage: create list title.
    :params:
    title str
    project_title str

    :return:
    message: str
    status : True / False

    """

    if request.method == "POST":
        params = json.loads(request.body)
        schema = list_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        list_title = params.get('title')
        project_title = params.get('project_title')
        project_object = Project.objects.get(title=project_title)

        queryset = List.objects.filter(title=list_title)

        if queryset.exists():
            return JsonResponse({
                "name": "Already Available.",
                "status": False
            })

        instance = List.objects.create(title=list_title, project=project_object)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "GET":
        params = request.GET

        title = params.get("title")
        pk = params.get("id")
        page_number = params.get("page_number", 1)
        page_per_entries = params.get("page_per_entries", 2)
        kwargs = {}
        response = []

        if pk: kwargs['id'] = pk
        if title: kwargs["title"] = title

        queryset = List.objects.filter(**kwargs)
        queryset, pagination_dict = pagination(queryset, page_per_entries, page_number)

        for instance in queryset:
            response.append(instance.get_json())

        return JsonResponse({
            "message": "Success",
            "status": True,
            "pagination": pagination_dict,
            "data": response,
        })

    if request.method == 'PATCH':
        params = json.loads(request.body)
        schema = list_update_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        title = params.get('title')
        kwargs = {}

        if title: kwargs['title'] = title

        List.objects.filter(id=pk).update(**kwargs)
        instance = List.objects.get(id=pk)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "DELETE":
        List.objects.filter(id=pk).delete()

        return JsonResponse({
            "message": "Success",
            "status": True
        })


def task_priority(request, pk=None):
    """
    Usage: create task priority.
    :params:
    title str

    :return:
    message: str
    status : True / False

    """

    if request.method == "POST":
        params = json.loads(request.body)
        schema = task_priority_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        title = params.get('title')

        queryset = TaskPriority.objects.filter(title=title)

        if queryset.exists():
            return JsonResponse({
                "name": "Already Available.",
                "status": False
            })

        instance = TaskPriority.objects.create(title=title)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "GET":
        params = request.GET

        title = params.get("title")
        pk = params.get("id")
        page_number = params.get("page_number", 1)
        page_per_entries = params.get("page_per_entries", 2)
        kwargs = {}
        response = []

        if pk: kwargs['id'] = pk
        if title: kwargs["title"] = title

        queryset = TaskPriority.objects.filter(**kwargs)
        queryset, pagination_dict = pagination(queryset, page_per_entries, page_number)

        for instance in queryset:
            response.append(instance.get_json())

        return JsonResponse({
            "message": "Success",
            "status": True,
            "pagination": pagination_dict,
            "data": response,
        })

    if request.method == 'PATCH':
        params = json.loads(request.body)
        schema = task_priority_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        title = params.get('title')
        kwargs = {}

        if title: kwargs['title'] = title

        TaskPriority.objects.filter(id=pk).update(**kwargs)
        instance = TaskPriority.objects.get(id=pk)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "DELETE":
        TaskPriority.objects.filter(id=pk).delete()

        return JsonResponse({
            "message": "Success",
            "status": True
        })


def task(request, pk=None):
    """
    Usage: create task.
    :params:
    title str
    list title str
    priority_title str
    start date_field
    due date datetime
    description textfield
    assignee many-to-many with User

    :return:
    message: str
    status : True / False

    """

    if request.method == "POST":
        params = json.loads(request.body)
        schema = task_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        title = params.get('title')
        due_date = params.get('due_date')
        description = params.get('description')

        list_title = params.get('list_title')
        list_object = List.objects.get(title=list_title)

        priority_title = params.get('priority_title')
        priority_object = TaskPriority.objects.get(title=priority_title)

        assignee_ids = params.get('assignee')
        assignee_queryset = User.objects.filter(id=assignee_ids)

        instance = Task.objects.create(title=title, priority=priority_object, list=list_object,
                                       due_date=due_date, description=description)
        instance.assignee.set(assignee_queryset)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "GET":
        params = request.GET

        title = params.get("title")
        pk = params.get("id")
        page_number = params.get("page_number", 1)
        page_per_entries = params.get("page_per_entries", 2)
        kwargs = {}
        response = []

        if pk:
            kwargs['id'] = pk
            queryset = Task.objects.filter(assignee__id=pk)

        if title:
            kwargs["title"] = title
            queryset = Task.objects.filter(title=title)

        queryset, pagination_dict = pagination(queryset, page_per_entries, page_number)

        for instance in queryset:
            response.append(instance.get_json())

        return JsonResponse({
            "message": "Success",
            "status": True,
            "pagination": pagination_dict,
            "data": response,
        })

    if request.method == 'PATCH':
        params = json.loads(request.body)
        schema = task_update_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        title = params.get('title')
        kwargs = {}

        if title: kwargs['title'] = title

        Task.objects.filter(id=pk).update(**kwargs)
        instance = Task.objects.get(id=pk)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "DELETE":
        Task.objects.filter(id=pk).delete()

        return JsonResponse({
            "message": "Success",
            "status": True
        })


def subtask(request, pk=None):
    """
    Usage: create sub-task.
    :params:
    title str
    task_title str

    :return:
    message: str
    status : True / False

    """

    if request.method == "POST":
        params = json.loads(request.body)
        schema = subtask_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        subtask_title = params.get('title')
        task_title = params.get('task')
        task_object = Task.objects.get(title=task_title)

        queryset = SubTask.objects.filter(title=subtask_title)

        if queryset.exists():
            return JsonResponse({
                "name": "Already Available.",
                "status": False
            })

        instance = SubTask.objects.create(title=subtask_title, task=task_object)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "GET":
        params = request.GET

        title = params.get("title")
        # pk = params.get("id")
        page_number = params.get("page_number", 1)
        page_per_entries = params.get("page_per_entries", 2)
        kwargs = {}
        response = []

        if pk: kwargs['id'] = pk
        if title: kwargs["title"] = title

        queryset = SubTask.objects.filter(**kwargs)
        queryset, pagination_dict = pagination(queryset, page_per_entries, page_number)

        for instance in queryset:
            response.append(instance.get_json())

        return JsonResponse({
            "message": "Success",
            "status": True,
            "pagination": pagination_dict,
            "data": response,
        })

    if request.method == 'PATCH':
        params = json.loads(request.body)
        schema = subtask_update_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        title = params.get('title')
        kwargs = {}

        if title: kwargs['title'] = title

        SubTask.objects.filter(id=pk).update(**kwargs)
        instance = SubTask.objects.get(id=pk)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "DELETE":
        SubTask.objects.filter(id=pk).delete()

        return JsonResponse({
            "message": "Success",
            "status": True
        })


def setting(request, pk=None):
    """
    Usage: create setting .
    :params:
    workspace_title str
    project_title str
    team_member many-to-many User

    :return:
    message: str
    status : True / False

    """

    if request.method == "POST":
        params = json.loads(request.body)
        schema = setting_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        workspace_title = params.get('workspace_title')
        workspace_object = Workspace.objects.get(title=workspace_title)

        project_title = params.get('project_title')
        project_object = Project.objects.get(title=project_title)

        team_member_ids = params.get('team_member')
        team_member_queryset = User.objects.filter(id__in=team_member_ids)

        instance = Setting.objects.create(workspace=workspace_object, project=project_object)
        instance.team_member.set(team_member_queryset)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "GET":
        params = request.GET

        project_title = params.get("project_title")
        pk = params.get("id")
        page_number = params.get("page_number", 1)
        page_per_entries = params.get("page_per_entries", 2)
        kwargs = {}
        response = []

        if pk: kwargs['id'] = pk
        if project_title: kwargs["project_title"] = project_title

        queryset = Setting.objects.filter(**kwargs)
        queryset, pagination_dict = pagination(queryset, page_per_entries, page_number)

        for instance in queryset:
            response.append(instance.get_json())

        return JsonResponse({
            "message": "Success",
            "status": True,
            "pagination": pagination_dict,
            "data": response,
        })

    if request.method == 'PATCH':
        params = json.loads(request.body)
        schema = setting_update_schema()
        json_error = validate_json(params, schema)
        if json_error:
            return JsonResponse(missing_fields_validation(json_error))

        add_team_member = params.get('add_team_member')
        remove_team_member = params.get('remove_team_member')

        add_member = User.objects.filter(id__in=add_team_member)
        remove_member = User.objects.filter(id__in=remove_team_member)

        instance = Setting.objects.get(id=pk)

        if add_member: instance.team_member.add(*add_member)
        if remove_member: instance.team_member.remove(*remove_member)

        return JsonResponse({
            "message": "Success",
            "status": True,
            "data": instance.get_json()
        })

    if request.method == "DELETE":
        Setting.objects.filter(id=pk).delete()

        return JsonResponse({
            "message": "Success",
            "status": True
        })


# def show_data(request):
#     params = request.GET
#     pk = params.get("id")
#     # username = params.get('username')
#     response = []
#
#     queryset = Task.objects.filter(assignee__id=pk)
#     # queryset = Task.objects.filter(assignee__username=username)
#
#     for instance in queryset:
#         response.append({
#             "id": instance.id,
#             "task": instance.title
#         })
#
#     return JsonResponse({
#         "message": "Success",
#         "status": True,
#         "data": response
#     })
#
#
# def task_data(request):
#     params = request.GET
#     title = params.get('title')
#     response = []
#
#     queryset = Task.objects.filter(title=title)
#
#     for instance in queryset:
#         response.append({
#             "id": instance.id,
#             "list": instance.list.get_json(),
#             "priority": instance.priority.title,
#             "start_date": instance.start_date,
#             "due_date": instance.due_date,
#             "assignee": [x.username for x in instance.assignee.all()],
#         })
#
#     return JsonResponse({
#         "message": "Success",
#         "status": True,
#         "data": response
#     })


def read_excel_file(request):
    # Give the location of the file
    loc = "/home/sumit/project/Clickup/example.xls"
    response = []

    # To open workbook
    wb = xlrd.open_workbook(loc, "rb")
    sheet = wb.sheet_by_index(0)

    no_of_rows = sheet.nrows
    no_of_cols = sheet.ncols
    column = []

    for i in range(no_of_cols):
        column.append(sheet.cell_value(0, i))

    for row_no in range(no_of_rows):
        if row_no == 0:
            continue
        response.append({
            f'{column[0]}': sheet.cell_value(row_no, 0),
            f'{column[1]}': sheet.cell_value(row_no, 1),
            f'{column[2]}': sheet.cell_value(row_no, 2),
            f'{column[3]}': sheet.cell_value(row_no, 3),
            f'{column[4]}': sheet.cell_value(row_no, 4),
            f'{column[5]}': sheet.cell_value(row_no, 5),
            f'{column[6]}': sheet.cell_value(row_no, 6),
        })

    return JsonResponse({
        "message": "Success",
        "data": response
    })


# def show_data(request):
#     params = request.GET
#     date = params.get("date")
#     response = []
#
#     queryset = Task.objects.filter(start_date=date)
#
#     print(queryset)
#
#     for instance in queryset:
#         response.append({
#             "date": instance.start_date.date()
#         })
#
#         print(instance.start_date)
#         print(instance.start_date.date())
#
#     return JsonResponse({
#         "message": "Success",
#         "status": True,
#         "data": response
#     })


def show_data(request):
    params = json.loads(request.body)
    from_date = params.get("from_date")
    end_date = params.get("end_date")

    start = datetime.datetime.strptime(from_date, "%Y-%m-%d")
    end = datetime.datetime.strptime(end_date, "%Y-%m-%d")
    dates = [start + datetime.timedelta(days=x) for x in range(0, (end - start).days)]

    print(dates)

    response = []

    task_queryset = Task.objects.all()
    all_assignee = Project.objects.filter(id=1).last().team.all()

    for dt in dates:
        user_data = []

        for user in all_assignee:
            task_data = []

            for instance in user.task_set.filter(start_date__date=dt):
                task_data.append({
                    "task": instance.title
                })

            user_data.append({
                "id": user.id,
                "first_name": user.first_name,
                "task_count": task_queryset.filter(start_date__date=dt, assignee=user).count(),
                "task_data": task_data
            })

        response.append({
            "date": dt,
            "user_data": user_data
        })

    return JsonResponse({
        "message": "Success",
        "status": True,
        "data": response
    })
