def missing_fields_validation(missing_fields):
    default_validation = "Required missing fields"
    default_message = "validation error"
    default_error_code = 400

    return dict(
        validation=default_validation,
        message=default_message,
        error_code=default_error_code,
        status=False,
        data=missing_fields
    )