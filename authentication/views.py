import json

import pytz
from django.http import JsonResponse


from django.contrib.auth import authenticate, login
from django.contrib.auth import logout

from clickup.datetime import get_now
from authentication.compose import generate_random_number, trigger_email
from authentication.models import Signup, User, ValidateToken

from authentication.schema import *
from base.compose import missing_fields_validation
from base.schemaValidator import validate_json
from django.views.decorators.http import require_http_methods

import jwt
import pytz
from django.conf import settings
from datetime import timedelta, datetime
from authentication.decorators import valid_token_required


# Create your views here.


@require_http_methods("POST")
def login_user(request):
    """
    Usage: login User.
    :params:
    username str
    password str
    :return:
    message: str
    status : True / False
    """

    params = json.loads(request.body)
    schema = sign_in_schema()
    json_error = validate_json(params, schema)
    if json_error: return JsonResponse(missing_fields_validation(json_error))

    username = params.get('username')
    password = params.get('password')
    response = {}

    queryset = User.objects.filter(username=username)

    if not queryset.exists():
        return JsonResponse(dict(
            message="Account is Not Available",
            status=False
        ))

    user = authenticate(username=username, password=password)

    if user is None:
        return JsonResponse(dict(
            message="Error.",
            status=False
        ))

    login(request, user)
    instance = User.objects.get(username=username)
    exp_time = get_now() + timedelta(minutes=1)
    print("exp_time", exp_time)
    epoch = exp_time.astimezone(pytz.utc)

    payload = {
        "user_id": instance.id,
        "exp": epoch
    }

    key = settings.SECRET_KEY
    access_token = jwt.encode(payload, key, algorithm="HS256").decode("utf-8")
    print(access_token)

    ValidateToken.objects.create(user=instance, token=access_token, exp_time=exp_time)

    response['user_data'] = {
        "id": instance.id,
        "first_name": instance.first_name,
        "last_name": instance.last_name,
        }

    response['token_data'] = {
        "access_token": access_token
        }
    print(response)

    return JsonResponse(dict(
        message="Login Successfully",
        status=True,
        data=response,
    ))


@require_http_methods("POST")
def create_user_account(request):
    """
    Usage: To onboard new user.
    :params:
    username str
    password str
    purpose str "SIGN_UP" / "FORGET_PASSWORD"
    :return:
    message: str
    status : True / False
    """

    params = json.loads(request.body)
    schema = create_user_account_schema()
    json_error = validate_json(params, schema)
    if json_error: return JsonResponse(missing_fields_validation(json_error))

    username = params.get('username')
    password = params.get('password')
    first_name = params.get('first_name')
    last_name = params.get('last_name')

    queryset = User.objects.filter(username=username, first_name=first_name, last_name=last_name)

    otp = generate_random_number(4)
    subject = "Clickup | SIGN UP"
    message = f'This is your one time password {otp}'

    if queryset.exists():
        instance = queryset.last()

        if instance.is_active:
            return JsonResponse(dict(
                message="User Already Exists.",
                status=False
            ))
    else:
        trigger_email(subject, message, [username])
        Signup.objects.filter(email=username).delete()
        Signup.objects.create(email=username, otp=otp)

    user = User.objects.create(username=username, first_name=first_name, last_name=last_name)
    user.set_password(password)
    user.is_active = False
    user.save()

    return JsonResponse({
        "message": "OTP Sent Successfully",
        "status": True
    })


@require_http_methods("POST")
def validate_otp(request):
    """
    Usage: To validate OTP.
    :params:
    username str
    purpose str "SIGN_UP" / "FORGET_PASSWORD"
    otp str
    :return:
    message: str
    status : True / False
    """

    params = json.loads(request.body)
    schema = validate_email_otp_schema()
    json_error = validate_json(params, schema)
    if json_error: return JsonResponse(missing_fields_validation(json_error))

    username = params.get('username')
    purpose = params.get('purpose')
    otp = params.get('otp')

    queryset = Signup.objects.filter(email=username, otp=otp).order_by('-created')

    if not queryset.exists():
        return JsonResponse(dict(
            message="OTP Not Found.",
            status=False
        ))

    instance = queryset.first()

    if instance.is_utilized: return JsonResponse(dict(
        message="OTP is already used.", status=False))

    created = instance.created
    diff = get_now() - created
    total_seconds = diff.total_seconds()

    if total_seconds > 120:
        return JsonResponse({
            "message": "OTP Expired",
            "status": False
        })

    user = User.objects.get(username=username)
    instance.is_utilized = True

    if purpose == "SIGN_UP":
        user.is_active = True
        user.save()
        message = "Account Verified And User Created Successfully"

    elif purpose == "FORGET_PASSWORD":
        user.is_active = True
        user.save()
        message = "OTP Verified Successfully"

    return JsonResponse({
        "message": message,
        "status": True,
    })


@require_http_methods("POST")
def forget_password(request):
    """
    Usage: forget_password
    :params:
    username str
    purpose str "SIGN_UP" / "FORGET_PASSWORD"
    :return:
    message: str
    status : True / False
    """
    params = json.loads(request.body)
    schema = forget_password_schema()
    json_error = validate_json(params, schema)
    if json_error: return JsonResponse(missing_fields_validation(json_error))

    username = params.get('username')
    purpose = params.get('purpose')

    queryset = User.objects.filter(username=username)

    if not queryset.exists():
        return JsonResponse(dict(
            message="Username is Not Available",
            status=False
        ))

    otp = generate_random_number(4)
    subject = "Clickup | Forget password"
    message = f'This is your one time password {otp}'

    trigger_email(subject, message, [username])

    Signup.objects.filter(email=username, purpose=purpose).delete()
    Signup.objects.create(email=username, otp=otp, purpose=purpose)

    return JsonResponse({
        "message": "OTP Sent Successfully",
        "status": True
    })


@require_http_methods("POST")
def reset_password(request):
    """
    Usage: reset_password.
    :params:
    username str
    password str
    :return:
    message: str
    status : True / False
    """
    params = json.loads(request.body)
    schema = reset_password_schema()
    json_error = validate_json(params, schema)
    if json_error: return JsonResponse(missing_fields_validation(json_error))

    username = params.get('username')
    password = params.get('password')
    otp = params.get('otp')

    queryset = Signup.objects.filter(email=username, otp=otp).order_by('-created')

    if not queryset.exists():
        return JsonResponse(dict(
            message="OTP Not Found.",
            status=False
        ))

    instance = queryset.first()

    created = instance.created
    diff = get_now() - created
    total_seconds = diff.total_seconds()

    if total_seconds > 300:
        return JsonResponse({
            "message": "OTP Expired",
            "status": False
        })

    subject = "Clickup | Reset password"
    message = "Password Reset Successfully"
    trigger_email(subject, message, [username])

    user = User.objects.get(username=username)
    user.set_password(password)
    user.is_active = True
    user.save()

    return JsonResponse({
        "message": "Password Reset Successfully",
        "status": True
    })


@valid_token_required
def logout_view(request):
    """
    Usage: login User.
    :params:
    logout request

    :return:
    message: str
    status : True / False
    """

    logout(request)

    return JsonResponse({
        "message": "Account Logout Successfully",
        "status": True
    })


# def validate_token(request):
