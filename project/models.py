from django.db import models
from base.models import TitleMixin
from authentication.models import User
from clickup.models import BaseModel


class Company(TitleMixin):
    pass

    def __str__(self):
        return self.title

    def get_json(self):
        return dict(
            id=self.id,
            title=self.title
        )

    class Meta:
        ordering = ['-id']


class Workspace(TitleMixin):
    company = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_json(self):
        return dict(
            id=self.id,
            title=self.title,
            company=self.company.get_json()
        )

    class Meta:
        ordering = ['-id']


class Project(TitleMixin):
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)
    team = models.ManyToManyField(User, blank=True)

    def __str__(self):
        return self.title

    def get_json(self):
        return dict(
            id=self.id,
            title=self.title,
            workspace=self.workspace.get_json()
        )

    class Meta:
        ordering = ['-id']


class List(TitleMixin):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_json(self):
        return dict(
            id=self.id,
            title=self.title,
            project=self.project.get_json()
        )

    class Meta:
        ordering = ['-id']


class TaskPriority(TitleMixin):
    pass

    def __str__(self):
        return self.title

    def get_json(self):
        return dict(
            id=self.id,
            title=self.title
        )

    class Meta:
        ordering = ['-id']


class Task(BaseModel, TitleMixin):
    list = models.ForeignKey(List, on_delete=models.CASCADE)
    start_date = models.DateTimeField(null=True, blank=True)
    due_date = models.DateField(null=True, blank=True)
    assignee = models.ManyToManyField(User, blank=True)
    priority = models.ForeignKey(TaskPriority, on_delete=models.CASCADE, null=True, blank=True)
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.title

    def get_json(self):
        assignee = [{
            "id": x.id,
            "full_name": x.get_full_name()

        } for x in self.assignee.all()]
        return dict(
            id=self.id,
            title=self.title,
            assignee=assignee,
            priority=self.priority.get_json(),
            list=self.list.get_json()
        )

    class Meta:
        ordering = ['-id']


class SubTask(TitleMixin):
    task = models.ForeignKey(Task, on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    def get_json(self):
        return dict(
            id=self.id,
            title=self.title,
            task=self.task.get_json()
        )

    class Meta:
        ordering = ['-id']


class Setting(models.Model):
    workspace = models.ForeignKey(Workspace, on_delete=models.CASCADE)
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    team_member = models.ManyToManyField(User)

    def get_json(self):
        team_member = [{
            "id": x.id,
            "full_name": x.get_full_name()

        } for x in self.team_member.all()]
        return dict(
            id=self.id,
            project=self.project.get_json(),
            team_member=team_member
        )

    class Meta:
        ordering = ['-id']

