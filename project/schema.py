def company_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
        },
        "required": ["title"]
    }
    return schema


def workspace_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
            "company": {"type": "string"},
        },
        "required": ["title", "company"]
    }
    return schema


def workspace_update_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
        },
        "required": ["title"]
    }
    return schema


def project_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
            "workspace": {"type": "string"},
        },
        "required": ["title", "workspace"]
    }
    return schema


def project_update_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
        },
        "required": ["title"]
    }
    return schema


def list_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
            "project": {"type": "string"},
        },
        "required": ["title", "project"]
    }
    return schema


def list_update_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
        },
        "required": ["title"]
    }
    return schema


def task_priority_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
        },
        "required": ["title"]
    }
    return schema


def task_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
            "list_title": {"type": "string"},
            "due_date": {"type": "string"},
            "assignee": {"type": "array"},
            "priority_title": {"type": "string"},
            "description": {"type": "string"},
        },
        "required": ["title", "list_title", "due_date", "assignee", "priority_title", "description"]
    }
    return schema


def task_update_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
        },
        "required": ["title"]
    }
    return schema


def subtask_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
            "task": {"type": "string"},
        },
        "required": ["title", "task"]
    }
    return schema


def subtask_update_schema():
    schema = {
        "type": "object",
        "properties": {
            "title": {"type": "string"},
        },
        "required": ["title"]
    }
    return schema


def setting_schema():
    schema = {
        "type": "object",
        "properties": {
            "workspace": {"type": "string"},
            "project": {"type": "string"},
            "team_member": {"type": "array"},
        },
        "required": ["workspace", "project", "team_member" ]
    }
    return schema


def setting_update_schema():
    schema = {
        "type": "object",
        "properties": {
            "team_member": {"type": "array"},
        },
        "required": ["team_member"]
    }
    return schema
