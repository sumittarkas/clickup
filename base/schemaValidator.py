import json

from jsonschema import Draft7Validator


def validate_json(params, schema):
    validator = Draft7Validator(schema)
    validation_errors = sorted(validator.iter_errors(params), key=lambda e: e.path)

    errors = []

    for error in validation_errors:
        message = error.message
        if error.path:
            message = "[{}] {}".format(
                ".".join(str(x) for x in error.absolute_path), message
            )

        errors.append(message)
    return errors
