from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class TitleMixin(models.Model):
    title = models.CharField(max_length=255)

    class Meta:
        abstract = True


class UserOneToOneMixin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class UserMixin(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        abstract = True
