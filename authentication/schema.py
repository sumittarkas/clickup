
def create_user_account_schema():
    schema = {
        "type": "object",
        "properties": {
            "username": {"type": "string"},
            "password": {"type": "string"},
        },
        "required": ["username", 'password']
    }

    return schema


def validate_email_otp_schema():
    schema = {
        "type": "object",
        "properties": {
            "username": {"type": "string"},
            "otp": {"type": "string"},
            "purpose": {
                "enum": ["SIGN_UP", "FORGET_PASSWORD"]
            },
        },
        "required": ["username", 'purpose', 'otp']
    }

    return schema


def forget_password_schema():
    schema = {
        "type": "object",
        "properties": {
            "username": {"type": "string"},
            "purpose": {"type": "string"},
        },
        "required": ["username", 'purpose']
    }

    return schema


def reset_password_schema():
    schema = {
        "type": "object",
        "properties": {
            "username": {"type": "string"},
            "password": {"type": "string"},
        },
        "required": ["username", 'password']
    }

    return schema


def sign_in_schema():
    schema = {
        "type": "object",
        "properties": {
            "username": {"type": "string"},
            "password": {"type": "string"},
        },
        "required": ["username", 'password']
    }

    return schema










