from django.contrib import admin
from .models import *


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ["id", "title"]


@admin.register(Workspace)
class WorkspaceAdmin(admin.ModelAdmin):
    list_display = ["id", "title"]


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ["id", "title"]


@admin.register(List)
class ListAdmin(admin.ModelAdmin):
    list_display = ["id", "title"]


@admin.register(TaskPriority)
class TaskPriorityAdmin(admin.ModelAdmin):
    list_display = ["id", "title"]


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "priority",
                    "start_date", "due_date",  "created", "modified"]


@admin.register(SubTask)
class SubTaskAdmin(admin.ModelAdmin):
    list_display = ["id", "title", "task"]


@admin.register(Setting)
class SettingAdmin(admin.ModelAdmin):
    list_display = ["id", "workspace", "project"]








