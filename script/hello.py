import sys
import os
project_path = os.path.split(os.path.dirname(os.path.realpath(__file__)))[-2]  # path to manage.py
print("project_path", project_path)
sys.path.append(project_path)
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'clickup.settings')

import django
django.setup()


def print_hello():
    print("Hello")


print_hello()
