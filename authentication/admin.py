from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin
from .models import *

# Register your models here.


@admin.register(Signup)
class SignupAdmin(admin.ModelAdmin):
    list_display = ["id", "email", "otp"]


@admin.register(User)
class UserAdminChild(UserAdmin):
    fieldsets = (
        ("Base Info", {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name',
                                         'email')}),
        ("Setting", {'fields': ('task_capacity',)}),
        (_('Permissions'), {
            'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions'),
        }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )

    list_display = ('id', 'username', 'first_name', 'last_name', 'is_staff',)
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups', )
    search_fields = ('username', 'first_name', 'last_name', 'email', 'pk')
    ordering = ['-id']


@admin.register(ValidateToken)
class ValidateTokenAdmin(admin.ModelAdmin):
    list_display = ["user", "token", "exp_time"]