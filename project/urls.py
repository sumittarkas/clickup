from django.urls import path
from .views import *

urlpatterns = [
    path('company', company),
    path('company/<int:pk>', company),
    path('workspace', workspace),
    path('workspace/<int:pk>', workspace),
    path('project', project),
    path('project/<int:pk>', project),
    path('list_element', list_element),
    path('list_element/<int:pk>', list_element),
    path('task_priority', task_priority),
    path('task_priority/<int:pk>', task_priority),
    path('task', task),
    path('task/<int:pk>', task),
    path('subtask', subtask),
    path('subtask/<int:pk>', subtask),
    path('setting', setting),
    path('setting/<int:pk>', setting),
    path('show_data', show_data),
    # path('task_data', task_data),
    path('read_excel_file', read_excel_file)
]
